# SPSC Queue

Zig implementation of a lock-free single-producer, single-consumer queue, based on [the CppCon talk
by Charles Frasch (YouTube)](https://www.youtube.com/watch?v=K3P_Lmq6pw0).
